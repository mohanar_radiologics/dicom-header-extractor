package ccf.nrg.siemens.dicomheaderextractor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xnat.helpers.dicom.DicomHeaderDump;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import au.com.bytecode.opencsv.CSVReader;
import ccf.nrg.siemens.dicomheaderextractor.util.DicomConstants;
import ccf.nrg.siemens.dicomheaderextractor.util.DicomHeaderUtility;

/**
 * The Class SiemensDicomHeaderExtractor.
 * @author Atul
 */
@XnatPlugin(value = "dicomHeaderExtractionPlugin",
name = "Dicom HeaderExtraction Plugin",
log4jPropertiesFile="/dicomExtractionLog4j.properties")
public class SiemensDicomHeaderExtractor {

	/** The logger. */
	public static Logger logger =  LoggerFactory.getLogger(SiemensDicomHeaderExtractor.class);

	/** The siemens key map. */
	static Map<String, String> siemensKeyMap = new LinkedHashMap<String, String>();

	static {
		CSVReader reader = null;
		try {
			reader = new CSVReader(new InputStreamReader(
					SiemensDicomHeaderExtractor.class.getResourceAsStream("/SiemensHeadersBBRCPET.csv")));
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				siemensKeyMap.put(nextLine[0], nextLine[1]);
			}
		} catch (IOException e) {
			logger.info("Unable to load Siemens Headers from SiemensHeaders.csv" + e.getMessage());
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Unable to load SiemensHeaders.csv file");
					logger.error(e.getMessage());
					e.printStackTrace();
				}
		}
	}

	/**
	 * Gets the siemens dicom header info map.
	 *
	 * @param dcmDirPath
	 *            the dcm dir path
	 * @return the siemens dicom header info map
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws ParseException
	 * @throws Exception
	 */
	public Map<String, String> getSiemensDicomHeaderInfoMap(String dcmDirPath) throws IOException, ParserConfigurationException, SAXException, ParseException {
		logger.debug("Extracting Dicom headers for the files present under "+dcmDirPath);
		Map<String, String> siemensValuesMap = new LinkedHashMap<String, String>();
		try {
			// As per Mike Harms comments, modified plugin to extract information from the dicom file where Image/Instance Number=1.
			String earliestAcqTimeDicomFilePath = null;

			// Get all the files with .dcm extension
			File[] filesWithDCMExtension = new File(dcmDirPath).listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(".dcm");
				}
			});
			// Get all the files without any extension. Special use-case for UCLA files.
			File[] filesWithoutExtension = new File(dcmDirPath).listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return !name.endsWith(".dcm") && !name.endsWith(".xml");
				}
			});

			logger.debug("Extracting information from all dicom files");
			// Process All dicom files for specific attributes
			if (filesWithDCMExtension != null && filesWithDCMExtension.length >0)
				earliestAcqTimeDicomFilePath=getAttributesFromAllFiles(filesWithDCMExtension, siemensValuesMap);
			if (filesWithoutExtension != null && filesWithoutExtension.length >0)
			{
				if(earliestAcqTimeDicomFilePath==null)
					earliestAcqTimeDicomFilePath=getAttributesFromAllFiles(filesWithoutExtension, siemensValuesMap);
				else
					getAttributesFromAllFiles(filesWithoutExtension, siemensValuesMap);
			}

			if (filesWithDCMExtension != null && filesWithDCMExtension.length > 0)
				processFile(earliestAcqTimeDicomFilePath!=null?earliestAcqTimeDicomFilePath:filesWithDCMExtension[0].getPath(), siemensValuesMap);
			if (filesWithoutExtension != null && filesWithoutExtension.length > 0)
				processFile(earliestAcqTimeDicomFilePath!=null?earliestAcqTimeDicomFilePath:filesWithoutExtension[0].getPath(), siemensValuesMap);

			//printDicomHeaders(siemensValuesMap);
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			logger.error(ExceptionUtils.getStackTrace(e));
			throw e;
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			logger.error(ExceptionUtils.getStackTrace(e));
			throw e;
		}
		logger.debug("Returning HeaderInfo Map");
		return siemensValuesMap;
	}

	/**
	 * Gets the first dicom file path from catalog.
	 *
	 * @param dcmDirPath the dcm dir path
	 * @return the first dicom file path from catalog
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws SAXException the SAX exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private String getFirstDicomFilePathFromCatalog(String dcmDirPath)
			throws ParserConfigurationException, SAXException, IOException {
		String firstDicomFilePath=null;
		File[] catalogFile = new File(dcmDirPath).listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith("_catalog.xml");
			}
		});
		if(catalogFile!=null && catalogFile.length >0)
		{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(catalogFile[0]);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("cat:entry");
			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					firstDicomFilePath= eElement.getAttribute("ID");
					break;
				}
			}
			if(firstDicomFilePath!=null)
			{
				firstDicomFilePath=firstDicomFilePath.replace("null", dcmDirPath);
			}
		}
		return firstDicomFilePath;
	}

	/**
	 * Gets the attributes from all files.
	 *
	 * @param files
	 *            the files
	 * @param siemensValuesMap
	 *            the siemens values map
	 * @return the attributes from all files
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ParseException
	 */
	private String getAttributesFromAllFiles(File[] files, Map<String, String> siemensValuesMap)
			throws FileNotFoundException, IOException, ParseException {
		Map<Integer, Integer> acquisitionVsInstanceNumberMap = new HashMap<Integer, Integer>();
		String earliestAcqTimeDicomFilePath=null;
		Boolean useAcquisitionNumber = true;
		ArrayList<String> imageComments=new ArrayList<String>(files.length+1);
		for (int i = 0; i < files.length+1; i++) {
			imageComments.add("");
		}
		for (int i = 0; i < files.length; i++) {
			Map<String, String> headerValuesMap = getAllHeadersMap(files[i].getPath(), Boolean.FALSE);
			getEarliestAcqTimeValue(siemensValuesMap, headerValuesMap);
			getDiffusionMaxBValue(siemensValuesMap, headerValuesMap);
			getImageCommentsValue(imageComments, headerValuesMap, acquisitionVsInstanceNumberMap,
					useAcquisitionNumber);
			if(earliestAcqTimeDicomFilePath==null)
				earliestAcqTimeDicomFilePath=Integer.valueOf(headerValuesMap.get(DicomConstants.INSTANCE_NUMBER_TAG))==1?files[i].getPath():null;
		}

		Set<String> uniqueComments=new HashSet<String>(imageComments.subList(1, imageComments.size()));
		if(uniqueComments.size() == 1 && !DicomConstants.EMPTY_STRING.equals(uniqueComments.iterator().next()))
		{
			siemensValuesMap.put(DicomConstants.IMAGE_COMMENTS_KEY, uniqueComments.toArray(new String[0])[0]);
		}
		else if(uniqueComments.size() > 1)
		{
			siemensValuesMap.put(DicomConstants.IMAGE_COMMENTS_KEY, "<b>View/Download XML to view image comments.</b>");
			for (int i = 1; i < imageComments.size(); i++) {
				siemensValuesMap.put("Image Comments[" + i + "]", imageComments.get(i));
			}
		}
		return earliestAcqTimeDicomFilePath;
	}

	/**
	 * Gets the earliest acq time value.
	 *
	 * @param siemensValuesMap the siemens values map
	 * @param headerValuesMap the header values map
	 * @return the earliest acq time value
	 * @throws ParseException the parse exception
	 */
	private void getEarliestAcqTimeValue(Map<String, String> siemensValuesMap, Map<String, String> headerValuesMap) throws ParseException {
		String acqTimeFromDicom=headerValuesMap.get(DicomConstants.ACQUISITION_TIME_TAG);
		String acqTimeFromExistingData=siemensValuesMap.get(DicomConstants.ACQUISITION_TIME_KEY);
		SimpleDateFormat format= new SimpleDateFormat("HH:mm:ss");
		Date dateFromDicom=null;
		Date dateFromHeaderMap=null;
		if(acqTimeFromDicom!=null && !DicomConstants.SPACE_STRING.equals(acqTimeFromDicom))
			dateFromDicom=format.parse(acqTimeFromDicom);
		if(acqTimeFromExistingData!=null && !DicomConstants.SPACE_STRING.equals(acqTimeFromExistingData))
			dateFromHeaderMap=format.parse(acqTimeFromExistingData);

		if (dateFromDicom ==null && dateFromHeaderMap != null) {
			siemensValuesMap.put(DicomConstants.ACQUISITION_TIME_KEY,acqTimeFromExistingData);
		}
		else if (dateFromDicom != null
				&& dateFromHeaderMap != null
				&& dateFromDicom.before(dateFromHeaderMap)) {
			siemensValuesMap.put(DicomConstants.ACQUISITION_TIME_KEY,acqTimeFromDicom);
		}
	}

	/**
	 * Gets the diffusion max B value.
	 *
	 * @param siemensValuesMap
	 *            the siemens values map
	 * @param headerValuesMap
	 *            the header values map
	 * @return the diffusion max B value
	 */
	private void getDiffusionMaxBValue(Map<String, String> siemensValuesMap, Map<String, String> headerValuesMap) {
		if ((siemensValuesMap.get(DicomConstants.DIFFUSION_MAXIMUM_B_KEY) == null || DicomConstants.SPACE_STRING
				.equals(siemensValuesMap.get(DicomConstants.DIFFUSION_MAXIMUM_B_KEY).trim()))
				&& headerValuesMap.get(DicomConstants.DIFFUSION_MAX_B_TAG) != null) {
			siemensValuesMap.put(DicomConstants.DIFFUSION_MAXIMUM_B_KEY,
					Double.valueOf(headerValuesMap.get(DicomConstants.DIFFUSION_MAX_B_TAG)).toString());
		} else if (siemensValuesMap.get(DicomConstants.DIFFUSION_MAXIMUM_B_KEY) != null
				&& headerValuesMap.get(DicomConstants.DIFFUSION_MAX_B_TAG) != null
				&& Double.valueOf(siemensValuesMap.get(DicomConstants.DIFFUSION_MAXIMUM_B_KEY)) < Double
						.valueOf(headerValuesMap.get(DicomConstants.DIFFUSION_MAX_B_TAG))) {
			siemensValuesMap.put(DicomConstants.DIFFUSION_MAXIMUM_B_KEY,
					Double.valueOf(headerValuesMap.get(DicomConstants.DIFFUSION_MAX_B_TAG)).toString());
		}
	}

	/**
	 * Gets the image comments value.
	 *
	 * @param imageCommentsMap
	 *            the image comments map
	 * @param headerValuesMap
	 *            the header values map
	 * @param acquisitionVsInstanceNumberMap
	 *            the acquisition vs instance number map
	 * @param useAcquisitionNumber
	 *            the use acquisition number
	 * @return the image comments value
	 */
	private void getImageCommentsValue(ArrayList<String> imageComments, Map<String, String> headerValuesMap,
			Map<Integer, Integer> acquisitionVsInstanceNumberMap, boolean useAcquisitionNumber) {
		if (headerValuesMap.get(DicomConstants.IMAGE_COMMENTS_TAG) != null) {
		//		&& imageCommentsMap.get(headerValuesMap.get(DicomConstants.IMAGE_COMMENTS_TAG)) == null) {
			/**
			 * Use Acquisition number for index if it is unique for every dicom file.
			 * Otherwise, use Instance number.
			 */
			if (useAcquisitionNumber) {
				Integer acqNumKey = Integer.valueOf(headerValuesMap.get(DicomConstants.ACQUISITION_NUMBER_TAG));
				if (acquisitionVsInstanceNumberMap.containsKey(acqNumKey)) {
					useAcquisitionNumber = false;
					imageComments=replaceAcqNumWithInstanceNum(imageComments, acquisitionVsInstanceNumberMap);
				} else {
					acquisitionVsInstanceNumberMap.put(
							Integer.valueOf(headerValuesMap.get(DicomConstants.ACQUISITION_NUMBER_TAG)),
							Integer.valueOf(headerValuesMap.get(DicomConstants.INSTANCE_NUMBER_TAG)));
				}
			}

			if (useAcquisitionNumber)
				imageComments.set(Integer.valueOf(headerValuesMap.get(DicomConstants.ACQUISITION_NUMBER_TAG)),escapeImageComments(headerValuesMap.get(DicomConstants.IMAGE_COMMENTS_TAG)));
			else
				imageComments.set(Integer.valueOf(headerValuesMap.get(DicomConstants.INSTANCE_NUMBER_TAG)),escapeImageComments(headerValuesMap.get(DicomConstants.IMAGE_COMMENTS_TAG)));
		}
	}

	/**
	 * Escape image comments.
	 *
	 * @param imageComments the image comments
	 * @return the string
	 */
	private String escapeImageComments(String imageComments)
	{
		return imageComments.replaceAll("", "?");
	}

	/**
	 * Replace acq num with instance num.
	 *
	 * @param imageComments
	 *            the image comments map
	 * @param acquisitionVsInstanceNumberMap
	 *            the acquisition vs instance number map
	 */
	private ArrayList<String> replaceAcqNumWithInstanceNum(ArrayList<String> imageComments,
			Map<Integer, Integer> acquisitionVsInstanceNumberMap) {
		ArrayList<String> tempList=new ArrayList<String>(imageComments.size());

		for (Iterator<Integer> iterator = acquisitionVsInstanceNumberMap.keySet().iterator(); iterator.hasNext();) {
			Integer acqNumber = iterator.next();
			tempList.set(acquisitionVsInstanceNumberMap.get(acqNumber).intValue(),imageComments.get(acqNumber));
		}
		return tempList;
	}

	/**
	 * Process file.
	 *
	 * @param filePath
	 *            the file path
	 * @param siemensValuesMap
	 *            the siemens values map
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ParseException
	 */
	private void processFile(String filePath, Map<String, String> siemensValuesMap)
			throws FileNotFoundException, IOException, ParseException {
		Map<String, String> headerValuesMap = getAllHeadersMap(filePath, Boolean.TRUE);
		if (headerValuesMap != null)
			getSiemensHeadersMap(headerValuesMap, siemensValuesMap);
	}

	/**
	 * Gets the all headers map.
	 *
	 * @param filePath
	 *            the file path
	 * @return the all headers map
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws ParseException
	 */
	private Map<String, String> getAllHeadersMap(String filePath, Boolean skipMultiFileAttributes) throws IOException, FileNotFoundException, ParseException {
		DicomHeaderDump d = new DicomHeaderDump(filePath);
		XFTTable xft = d.render();
		DicomHeaderUtility util = new DicomHeaderUtility();
		Map<String, String> headerValuesMap = util.createHeaderMap(xft,skipMultiFileAttributes);
		return headerValuesMap;
	}

	/**
	 * Gets the siemens headers map.
	 *
	 * @param headerValuesMap
	 *            the header values map
	 * @param siemensValuesMap
	 *            the siemens values map
	 * @return the siemens headers map
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void getSiemensHeadersMap(Map<String, String> headerValuesMap, Map<String, String> siemensValuesMap)
			throws IOException {
		for (Iterator<String> iterator = siemensKeyMap.keySet().iterator(); iterator.hasNext();) {
			String key = iterator.next();
			if (headerValuesMap.get(key) != null && !DicomConstants.EMPTY_STRING.equals(headerValuesMap.get(key))) {
				siemensValuesMap.put(
						siemensKeyMap.get(key).replaceAll(DicomConstants.SPACE_STRING, DicomConstants.UNDERSCORE),
						headerValuesMap.get(key));
			}
		}
	}

	/**
	 * Send notifications.
	 *
	 * @param subject
	 *            the subject
	 * @param message
	 *            the message
	 * @param emailAddress
	 *            the email address
	 */
	public void sendNotifications(String subject, String message, String[] emailAddress) {
		AdminUtils.sendUserHTMLEmail(subject, message, false, emailAddress);
	}

	/**
	 * Prints the dicom headers.
	 *
	 * @param valuesMap
	 *            the values map
	 */
	private void printDicomHeaders(Map<String, String> valuesMap) {
		for (Iterator<String> iterator = valuesMap.keySet().iterator(); iterator.hasNext();) {
			String key = iterator.next();
			System.out.println(key + " :: " + valuesMap.get(key));
		}
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws Exception
	 *             the exception
	 */
	public static void main(String... args) throws Exception {
		SiemensDicomHeaderExtractor s = new SiemensDicomHeaderExtractor();
		s.getSiemensDicomHeaderInfoMap("C:\\Users\\Atul\\Desktop\\HCD0184743_V1_A\\scans\\17_T1w_4e\\DICOM");

	}
}
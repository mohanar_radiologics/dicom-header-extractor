import ccf.nrg.siemens.dicomheaderextractor.SiemensDicomHeaderExtractor
import org.nrg.xdat.om.XnatExperimentdata
import org.nrg.xdat.om.XnatResourcecatalog
import org.nrg.xdat.om.XnatImagescandata
import groovy.io.FileType
import java.lang.reflect.Method
import org.nrg.xdat.om.XnatAddfield
import org.nrg.xdat.bean.XnatImagescandataBean
import org.nrg.xft.utils.SaveItemHelper
import org.nrg.xdat.om.XnatMrscandata

try {
    def dicomHeaderGen= new SiemensDicomHeaderExtractor()
    def experimentData = XnatExperimentdata.getXnatExperimentdatasById(dataId, user, false)
    def scans=experimentData.getSortedScans()
    for (scan in scans) 
    {
        if(scan instanceof XnatMrscandata)
        {
            def resources= scan.getFile()
            for(resource in resources)
            {
                def type = resource.getLabel()
                if("DICOM"== type || "secondary"==type)
                {
                    def dicomDir="/data/intradb/archive/"+externalId.toString()+"/arc001/"+experimentData.getLabel()+"/SCANS/"+scan.getId()+"/DICOM"
                    def basedir = new File(dicomDir)
					//println(dicomDir)
					def siemensValuesMap=dicomHeaderGen.getSiemensDicomHeaderInfoMap(dicomDir);
					processData(scan,siemensValuesMap)
                }
            }
        }
    }
} catch (Exception e) {
     println e
}

def processData(scan,siemensValuesMap)
{
		if(siemensValuesMap.get("Acquisition_Time")!=null)
        {
            scan.setStarttime(siemensValuesMap.get("Acquisition_Time"));
            siemensValuesMap.remove("Acquisition_Time")
        }
        if(siemensValuesMap.get("Acquisition_type")!=null)
        {
            scan.setParameters_acqtype(siemensValuesMap.get("Acquisition_type"));
            siemensValuesMap.remove("Acquisition_type")
        }
        if(siemensValuesMap.get("Scan_sequence")!=null)
        {
            scan.setParameters_scansequence(siemensValuesMap.get("Scan_sequence"));;
            siemensValuesMap.remove("Scan_sequence")
        }
        if(siemensValuesMap.get("Sequence_variant")!=null)
        {
            scan.setParameters_seqvariant(siemensValuesMap.get("Sequence_variant"));
            siemensValuesMap.remove("Sequence_variant")
        }
        if(siemensValuesMap.get("Scan_options")!=null)
        {
            scan.setParameters_scanoptions(siemensValuesMap.get("Scan_options"));
            siemensValuesMap.remove("Scan_options")
        }
        if(siemensValuesMap.get("Subject_position")!=null)
        {
            scan.setParameters_subjectposition(siemensValuesMap.get("Subject_position"));
            siemensValuesMap.remove("Subject_position")
        }
        if(siemensValuesMap.get("Pixel_bandwidth")!=null)
        {
            scan.setParameters_pixelbandwidth(new Double(siemensValuesMap.get("Pixel_bandwidth")));
            siemensValuesMap.remove("Pixel_bandwidth")
        }
        if(siemensValuesMap.get("In-plane_phase_encoding_direction")!=null)
        {
            scan.setParameters_inplanephaseencoding_direction(siemensValuesMap.get("In-plane_phase_encoding_direction"));
            siemensValuesMap.remove("In-plane_phase_encoding_direction")
        }
        if(siemensValuesMap.get("In-plane_phase_encoding_rotation")!=null)
        {
            scan.setParameters_inplanephaseencoding_rotation(siemensValuesMap.get("In-plane_phase_encoding_rotation"));
            siemensValuesMap.remove("In-plane_phase_encoding_rotation")
        }
        if(siemensValuesMap.get("Readout_sample_spacing")!=null)
        {
            scan.setParameters_readoutsamplespacing(new Double(siemensValuesMap.get("Readout_sample_spacing")).toString());
            siemensValuesMap.remove("Readout_sample_spacing")
        }
        if(siemensValuesMap.get("Echo_Spacing_(sec)")!=null)
        {
            scan.setParameters_echospacing(new Double(siemensValuesMap.get("Echo_Spacing_(sec)")));
            siemensValuesMap.remove("Echo_Spacing_(sec)")
        }
        if(siemensValuesMap.get("PhaseEncoding_direction_positive")!=null)
        {
            scan.setParameters_inplanephaseencoding_directionpositive(siemensValuesMap.get("PhaseEncoding_direction_positive"));
            siemensValuesMap.remove("PhaseEncoding_direction_positive")
        }
        if(siemensValuesMap.get("adFlipAngleDegree[1]")!=null)
        {
            scan.setParameters_diffusion_refocusflipangle(siemensValuesMap.get("adFlipAngleDegree[1]"));
            siemensValuesMap.remove("adFlipAngleDegree[1]")
        }
        if(siemensValuesMap.get("Software_Version(s)")!=null)
        {
            scan.setScanner_softwareversion(siemensValuesMap.get("Software_Version(s)"));
            siemensValuesMap.remove("Software_Version(s)")
        }
        if(siemensValuesMap.get("coil")!=null)
        {
            scan.setCoil(siemensValuesMap.get("coil").replaceAll('\"',''));
            siemensValuesMap.remove("coil")
        }
        if(siemensValuesMap.get("UUID")!=null)
        {
            scan.setFilenameuuid(siemensValuesMap.get("UUID"));
            siemensValuesMap.remove("UUID")
        }
        if(siemensValuesMap.get("Diffusion_maximum_b")!=null)
        {
            scan.setParameters_diffusion_bmax(siemensValuesMap.get("Diffusion_maximum_b"));
            siemensValuesMap.remove("Diffusion_maximum_b")
        }
        if(siemensValuesMap.get("Siemens_sSliceArray.asSlice[0].dInPlaneRot")!=null)
        {
            scan.setParameters_inplanephaseencoding_rotation(siemensValuesMap.get("Siemens_sSliceArray.asSlice[0].dInPlaneRot"));
        }
        for(e in siemensValuesMap)
        {
            def key="${e.key}"
            def value="${e.value}"
            XnatAddfield newField = new XnatAddfield();
            if(!key.startsWith("fMRI_Version"))
                newField.setName(key.replaceAll("_"," "));
            else
                newField.setName(key);
            newField.setAddfield(value);
            scan.addParameters_addparam(newField);
            siemensValuesMap.remove("${e.key}")
        }
         SaveItemHelper.authorizedSave(scan.getItem(),user,false,false,null)
}